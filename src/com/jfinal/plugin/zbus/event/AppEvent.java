/**
 * Copyright (c) 2015, 玛雅牛［李飞］ (myaniu@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfinal.plugin.zbus.event;

/**
 * 普通的增删查改事件基类。
 * @ClassName: AppEvent  
 * @author 李飞  
 * @since V1.0.0
 */
public abstract class AppEvent<T>{
	public static final String ACTION_CREATE = "create";
	public static final String ACTION_UPDATE = "update";
	public static final String ACTION_DELETE = "delete";
	
    private  String action;
    private  T data;
    private  long otime;
    private  final String source;
    
    public AppEvent(String source) {
    	this.source = source;
    	this.otime = System.currentTimeMillis();
    }
    
    public AppEvent(String source, String action, T data) {
    	this.source = source;
    	this.action = action;
    	this.data = data;
    	this.otime = System.currentTimeMillis();
    }
    
	public final String getAction() {
		return action;
	}



	public final void setAction(String action) {
		this.action = action;
	}



	public final T getData() {
		return data;
	}



	public final void setData(T data) {
		this.data = data;
	}



	public final long getOtime() {
		return otime;
	}



	public final void setOtime(long otime) {
		this.otime = otime;
	}



	public final String getSource() {
		return source;
	}

	/**
     * @Title: isAction  
     * @Description: 是否是某个action 
     * @param action
     * @return 
     * @since V1.0.0
     */
    public final boolean isAction(String action){
    	return this.action.equals(action);
    }
    
    /**
     * 判断是否是删除事件
     */
    public final boolean isDelete(){
    	return this.action.equals(ACTION_DELETE);
    }
    
    /**
     * 判断是否是更新事件
     */
    public final boolean isUpdate(){
    	return this.action.equals(ACTION_UPDATE);
    }
    
    /**
     * 判断是否是创建事件
     */
    public final boolean isCreate(){
    	return this.action.equals(ACTION_CREATE);
    }
}
